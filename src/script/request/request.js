import axios from 'axios';
import Vue from 'Vue'
import VueCookies from 'vue-cookies'
import cookie from "vue-cookies"
import router from '../../router/index';
Vue.use(VueCookies)
import { showLoading, hideLoading } from './loading';// 环境的切换
if (process.env.NODE_ENV == 'development') {
  axios.defaults.baseURL = 'https://www.xiaodaotianhei.com/xnxw-admin';
  axios.defaults.baseFileURL = '  https://www.xiaodaotianhei.com/fileserver/document/upload';
  axios.defaults.baseURL1 = 'https://www.xiaodaotianhei.com/xnxw';
} else if (process.env.NODE_ENV == 'debug') {
  axios.defaults.baseURL = '';
} else if (process.env.NODE_ENV == 'production') {
  axios.defaults.baseURL = 'https://www.xiaodaotianhei.com/xnxw-admin';
  axios.defaults.baseFileURL = '  https://www.xiaodaotianhei.com/fileserver/document/upload';
  axios.defaults.baseURL1 = 'https://www.xiaodaotianhei.com/xnxw';
}

//在 request 拦截器实现
axios.interceptors.request.use(
  config => {
    config.withCredentials = true // 允许携带token ,这个是解决跨域产生的相关问题
    config.timeout = 6000
    let token = cookie.get('token')
    if (token) {
      config.headers = {
        'access-token': token,
      }
    }

    if (config.url.indexOf('upload')>-1) {
      config.headers = {
        'Content-type': 'multipart/form-data',
        'X-Xnxw-Token': token
      }
    }

    return config
  },
  error => {
    return Promise.reject(error)
  }
)
//在 response 拦截器实现
axios.interceptors.response.use(
  response => {
    // 定时刷新access-token
    if (response.status === 200) {
      if (response.data.code === 200) {
        return Promise.resolve(response);
      } else if(response.data.code === 403){
        router.push({name:'Login'})
      }else {
        alert(response.data.message)
      }
    } else {
      return Promise.reject(response);
    }
  },
  error => {
    hideLoading();
    if (error.response.status) {
      switch (error.response.status) {
        case 401:
          router.replace({
            path: '/login',
            query: {redirect: router.currentRoute.fullPath}
          });
          break;
      }
    }
  }
)

// post请求
export function ajaxpost(url, params = {}, type) {
  //let token = VueCookies.get('tokenid');
  return new Promise((resolve, reject) => {
    axios.post(axios.defaults.baseURL + url, params)
      .then(res => {
        resolve(res.data);
      })
      .catch(err => {
        reject(err.data)
      })
  });
}
export function ajaxpostImg(url, params = {}, type) {
  return new Promise((resolve, reject) => {
    axios.post(axios.defaults.baseFileURL + url, params)
      .then(res => {
        resolve(res.data);
      })
      .catch(err => {
        reject(err.data)
      })
  });
}
export function ajaxpost1(url, params = {}, type) {
  //let token = VueCookies.get('tokenid');
  return new Promise((resolve, reject) => {
    axios.post(axios.defaults.baseURL + url, params)
      .then(res => {
        resolve(res.data);
      })
      .catch(err => {
        reject(err.data)
      })
  });
}
export function ajaxget(url, params) {
  return new Promise((resolve, reject) => {
    axios.get(axios.defaults.baseURL + url, {
      params: params
    })
      .then(res => {
        resolve(res.data);
      })
      .catch(err => {
        reject(err.data)
      })
  });
}

export function ajaxget1(url, params) {
  return new Promise((resolve, reject) => {
    axios.get(axios.defaults.baseURL1 + url, {
      params: params
    })
      .then(res => {
        resolve(res.data);
      })
      .catch(err => {
        reject(err.data)
      })
  });
}
