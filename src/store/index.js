import Vue from 'vue'
import Vuex from 'vuex'
import ElementUI from 'element-ui';

import {ajaxpost, ajaxget, ajaxget1, ajaxpost1,ajaxpostImg} from "../script/request/request.js"; // 引入封装的请求
import router from '../router/index';
import {
  SOME_MUTATION,
  CHANGE_LIST,
  CHANGE_TYPE,
  SCHOOL_LIST,
  FORBID_LIST,
  CHANGE_FORBID_LIST,
  INFO_LIST,
  CHANGE_STATUS,
  STREET_LIST,
  BACK_DETAIL
} from './mutation-types'
import cookie from "vue-cookies"

Vue.use(Vuex);
const debug = process.env.NODE_ENV !== 'production';
export default new Vuex.Store({
  state: {
    streetList:null,
    resetStatus: null,
    releaseList: null,
    backType: [],
    school: [],
    forbidList: null,
    infoList: null,
    backDetail:{}
  },
  getters: {
    backDetail: state => state.backDetail,
    streetList: state => state.streetList,
    resetStatus: state => state.resetStatus,
    releaseList: state => state.releaseList,
    backType: state => state.backType,
    school: state => state.school,
    forbidList: state => state.forbidList,
    infoList: state => state.infoList,
  },
  actions: {
    login({commit, state}, data) {
      ajaxpost('/admin/login', {
        userName: data.userName,
        password: data.password
      })
        .then((res) => {
          cookie.set('token', res.data.token)
          router.push({path: '/'})
        })
    },
    getList({commit, state}, data) {
      ajaxget('/admin/complaint/list', {objectType: data ? data : 0})
        .then((res) => {
          commit('SOME_MUTATION', res.data)
        })
        .catch(error => {
          console.log('出错了', error);
        })
    },
    deleteList({commit, state}, data) {
      ajaxpost('/admin/complaint/delete', data)
        .then((res) => {
          if (res.code == 200) {
            let goods = state.releaseList.filter(item => {
              return item.releaseId != data.objectId;
            })
            commit('CHANGE_LIST', goods)
          }
        })
        .catch(error => {
          console.log('出错了', error);
        })
    },
    deleteUser({commit, state}, data) {
      ajaxpost('/admin/forbid/' + data.userId, {})
        .then((res) => {
          if (res.code == 200) {
            let goods = state.releaseList.filter(item => {
              return item.releaseId != data.objectId;
            })
            commit('CHANGE_LIST', goods)
          }
        })
        .catch(error => {
          console.log('出错了', error);
        })
    },
    /*后街s*/
    getSchool({commit, state}, data) {
      ajaxget1('/school/list', {})
        .then((res) => {
          if (res.code == 200) {
            commit('SCHOOL_LIST', res.data)
          }
        })
        .catch(error => {
          console.log('出错了', error);
        })
    },
    getType({commit, state}, data) {
      ajaxget1('/index/topic_type_list', {type: 2})
        .then((res) => {
          if (res.code == 200) {
            commit('CHANGE_TYPE', res.data)
          }
        })
        .catch(error => {
          console.log('出错了', error);
        })
    },
    createStreet({commit, state}, data) {
      ajaxpost1('/back/street/create', data)
        .then((res) => {
          if (res.code == 200) {
            router.push({path: '/back/list'})
            commit('CHANGE_STATUS')
            ElementUI.Message({
              message: '创建成功',
              type: 'success'
            });
          }
        })
        .catch(error => {
          console.log('出错了', error);
        })
    },
    getBDetail({commit, state}, data) {
      ajaxget1('/back/street/detail/'+data)
        .then((res) => {
          if (res.code == 200) {
            commit('BACK_DETAIL',res.data)
          }
        })
        .catch(error => {
          console.log('出错了', error);
        })
    },
    getBackList({commit, state}, data) {
      ajaxget('/back/street/list', data)
        .then((res) => {
          commit('STREET_LIST', res.data)
        })
        .catch(error => {
          console.log('出错了', error);
        })
    },
    deleteBack({commit, state}, data) {
      ajaxpost('/back/street/delete/' + data.id, {})
        .then((res) => {
          if (res.code == 200) {
            debugger
            let goods=state.streetList
            goods.list=goods.list.filter(item => {
              return item.id != data.id;
            })
            commit('STREET_LIST', goods)
          }
        })
        .catch(error => {
          console.log('出错了', error);
        })
    },
    /*封号列表s*/
    getForbidList({commit, state}, data) {
      let key = data ? data : ''
      ajaxget('/admin/forbid_list?key=' + key, {})
        .then((res) => {
          if (res.code == 200) {
            commit('FORBID_LIST', res.data)
          }
        })
        .catch(error => {
          console.log('出错了', error);
        })
    },
    forbidUser({commit, state}, data) {
      ajaxpost('/admin/cancel_forbid/' + data.userId, {})
        .then((res) => {
          if (res.code == 200) {
            let forbidList = state.forbidList.filter(item => {
              return item.id != data.userId;
            })
            commit('CHANGE_FORBID_LIST', forbidList)
          }
        })
        .catch(error => {
          console.log('出错了', error);
        })
    },
    /*认证列表s*/
    getInfoList({commit, state}, data) {
      ajaxget('/admin/user_auth_audit_list', {})
        .then((res) => {
          if (res.code == 200) {
            commit('INFO_LIST', res.data)
          }
        })
        .catch(error => {
          console.log('出错了', error);
        })
    },
    authAudit({commit, state}, data) {
      ajaxpost('/admin/user_auth_audit', data)
        .then((res) => {
          if (res.code == 200) {
            let goods = state.infoList.filter(item => {
              return item.id != data.id;
            })
            commit('INFO_LIST', goods)
          }
        })
        .catch(error => {
          console.log('出错了', error);
        })
    },
  },

  upLoadImg({commit, state}, data) {
    ajaxpostImg('/1/1"', data)
      .then((res) => {
        if (res.code == 200) {
         debugger
        }
      })
      .catch(error => {
        console.log('出错了', error);
      })
  },
  mutations: {
    [STREET_LIST](state, data) {
      data.list.map(function (item,index) {
        item.srcList=[]
        item.releaseFiles.map(function (item1,index1) {
          return item.srcList.push('https://www.xiaodaotianhei.com/document/'+item1.fileRelativePath)
        })
      })
      state.streetList = data;
    },
    [BACK_DETAIL](state, data) {
      state.backDetail = data;
    },

    [CHANGE_STATUS](state, data) {
      state.resetStatus = 1;
    },
    [SOME_MUTATION](state, data) {
      state.releaseList = data;
    },
    [CHANGE_LIST](state, data) {
      state.releaseList = data;
    },
    [CHANGE_TYPE](state, data) {
      state.backType = data;
    },
    [SCHOOL_LIST](state, data) {
      state.school = data;
    },
    [FORBID_LIST](state, data) {
      state.forbidList = data;
    },
    [CHANGE_FORBID_LIST](state, data) {
      state.forbidList = data;
    },
    [INFO_LIST](state, data) {
      state.infoList = data;
    },
  },
  strict: debug,
})
